﻿using System.Data.Entity;
using Imorest.Models;

namespace Imorest.DAL
{
    public class DBImorestContext : DbContext
    {
        public DBImorestContext() : base ("DefaultConnection"){ }

        public System.Data.Entity.DbSet<Imorest.Models.RealEstateType> RealEstateTypes { get; set; }
        /*
public DbSet <RealEstateType> RealEstateTypes  {get; set;}

public DbSet<Alert> Alerts { get; set; }

public DbSet<Admnistrator> Admnistrators { get; set; }

public DbSet<Advertisement> Advertisements { get; set; }

public DbSet<Client> Clients { get; set; }

public DbSet<RealEstate> RealEstates { get; set; }

public DbSet<Address> Addresses { get; set; }*/
    }
}