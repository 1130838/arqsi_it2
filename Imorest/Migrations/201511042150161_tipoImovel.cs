namespace Imorest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tipoImovel : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.RealEstateTypes", "RealEstateTypesID");
            RenameColumn(table: "dbo.RealEstateTypes", name: "RealEstateTypes_realEstateTypeID", newName: "RealEstateTypesID");
            RenameIndex(table: "dbo.RealEstateTypes", name: "IX_RealEstateTypes_realEstateTypeID", newName: "IX_RealEstateTypesID");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.RealEstateTypes", name: "IX_RealEstateTypesID", newName: "IX_RealEstateTypes_realEstateTypeID");
            RenameColumn(table: "dbo.RealEstateTypes", name: "RealEstateTypesID", newName: "RealEstateTypes_realEstateTypeID");
            AddColumn("dbo.RealEstateTypes", "RealEstateTypesID", c => c.Int());
        }
    }
}
