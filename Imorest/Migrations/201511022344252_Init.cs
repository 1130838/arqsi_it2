namespace Imorest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        addressId = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 100),
                        coordinates = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.addressId);
            
            CreateTable(
                "dbo.Admnistrators",
                c => new
                    {
                        admnistratorID = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                        userID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.admnistratorID)
                .ForeignKey("dbo.Users", t => t.userID)
                .Index(t => t.userID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        userID = c.String(nullable: false, maxLength: 128),
                        password = c.String(),
                        email = c.String(),
                    })
                .PrimaryKey(t => t.userID);
            
            CreateTable(
                "dbo.Advertisements",
                c => new
                    {
                        advertisementID = c.Int(nullable: false, identity: true),
                        advertisementRef = c.String(),
                        realEstateId = c.Int(nullable: false),
                        price = c.Single(nullable: false),
                        clientId = c.Int(nullable: false),
                        realEstateTypeID = c.Int(nullable: false),
                        advertisementTypeID = c.Int(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.advertisementID)
                .ForeignKey("dbo.AdvertisementTypes", t => t.advertisementTypeID, cascadeDelete: true)
                .ForeignKey("dbo.Clients", t => t.clientId, cascadeDelete: true)
                .ForeignKey("dbo.RealEstates", t => t.realEstateId, cascadeDelete: true)
                .ForeignKey("dbo.RealEstateTypes", t => t.realEstateTypeID, cascadeDelete: true)
                .Index(t => t.realEstateId)
                .Index(t => t.clientId)
                .Index(t => t.realEstateTypeID)
                .Index(t => t.advertisementTypeID);
            
            CreateTable(
                "dbo.AdvertisementTypes",
                c => new
                    {
                        advertisementTypeID = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.advertisementTypeID);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        clientId = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                        address = c.String(maxLength: 100),
                        userID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.clientId)
                .ForeignKey("dbo.Users", t => t.userID)
                .Index(t => t.userID);
            
            CreateTable(
                "dbo.RealEstates",
                c => new
                    {
                        realEstateId = c.Int(nullable: false, identity: true),
                        area = c.Single(nullable: false),
                        addressId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.realEstateId)
                .ForeignKey("dbo.Addresses", t => t.addressId, cascadeDelete: true)
                .Index(t => t.addressId);
            
            CreateTable(
                "dbo.RealEstateTypes",
                c => new
                    {
                        realEstateTypeID = c.Int(nullable: false, identity: true),
                        designation = c.String(nullable: false, maxLength: 100),
                        RealEstateTypesID = c.Int(),
                        RealEstateTypes_realEstateTypeID = c.Int(),
                    })
                .PrimaryKey(t => t.realEstateTypeID)
                .ForeignKey("dbo.RealEstateTypes", t => t.RealEstateTypes_realEstateTypeID)
                .Index(t => t.RealEstateTypes_realEstateTypeID);
            
            CreateTable(
                "dbo.Alerts",
                c => new
                    {
                        alertID = c.Int(nullable: false, identity: true),
                        clientId = c.Int(nullable: false),
                        alertRef = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.alertID)
                .ForeignKey("dbo.Clients", t => t.clientId, cascadeDelete: true)
                .Index(t => t.clientId);
            
            CreateTable(
                "dbo.Parameters",
                c => new
                    {
                        parameterID = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 100),
                        price = c.Single(nullable: false),
                        area = c.Single(nullable: false),
                        AlertID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.parameterID)
                .ForeignKey("dbo.Alerts", t => t.AlertID, cascadeDelete: true)
                .Index(t => t.AlertID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Parameters", "AlertID", "dbo.Alerts");
            DropForeignKey("dbo.Alerts", "clientId", "dbo.Clients");
            DropForeignKey("dbo.Advertisements", "realEstateTypeID", "dbo.RealEstateTypes");
            DropForeignKey("dbo.RealEstateTypes", "RealEstateTypes_realEstateTypeID", "dbo.RealEstateTypes");
            DropForeignKey("dbo.Advertisements", "realEstateId", "dbo.RealEstates");
            DropForeignKey("dbo.RealEstates", "addressId", "dbo.Addresses");
            DropForeignKey("dbo.Advertisements", "clientId", "dbo.Clients");
            DropForeignKey("dbo.Clients", "userID", "dbo.Users");
            DropForeignKey("dbo.Advertisements", "advertisementTypeID", "dbo.AdvertisementTypes");
            DropForeignKey("dbo.Admnistrators", "userID", "dbo.Users");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Parameters", new[] { "AlertID" });
            DropIndex("dbo.Alerts", new[] { "clientId" });
            DropIndex("dbo.RealEstateTypes", new[] { "RealEstateTypes_realEstateTypeID" });
            DropIndex("dbo.RealEstates", new[] { "addressId" });
            DropIndex("dbo.Clients", new[] { "userID" });
            DropIndex("dbo.Advertisements", new[] { "advertisementTypeID" });
            DropIndex("dbo.Advertisements", new[] { "realEstateTypeID" });
            DropIndex("dbo.Advertisements", new[] { "clientId" });
            DropIndex("dbo.Advertisements", new[] { "realEstateId" });
            DropIndex("dbo.Admnistrators", new[] { "userID" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Parameters");
            DropTable("dbo.Alerts");
            DropTable("dbo.RealEstateTypes");
            DropTable("dbo.RealEstates");
            DropTable("dbo.Clients");
            DropTable("dbo.AdvertisementTypes");
            DropTable("dbo.Advertisements");
            DropTable("dbo.Users");
            DropTable("dbo.Admnistrators");
            DropTable("dbo.Addresses");
        }
    }
}
