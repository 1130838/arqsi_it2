namespace Imorest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addressLong : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RealEstates", "addressLong", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.RealEstates", "addressLong");
        }
    }
}
