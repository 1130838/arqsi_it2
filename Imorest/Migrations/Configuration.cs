namespace Imorest.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Imorest.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Imorest.Models.ApplicationDbContext context)
        {

            try
            {

                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);
                //var role = roleManager.FindByName("Client");
                ////
                //if (role == null)
                //{
                //    var roleNew = new IdentityRole();
                //    roleNew.Name = "Client";
                //    var result = roleManager.CreateAsync(roleNew);
                //}

                var role2 = roleManager.FindByName("Admin");
                //
                if (role2 == null)
                {
                    var roleNew = new IdentityRole();
                    roleNew.Name = "Admin";
                    var result = roleManager.Create(roleNew);
                }

                string UserNameTmp = "BigBoss@mail.com";
                var passwordHash = new PasswordHasher();
                string password = passwordHash.HashPassword("Password123@");
                context.Users.AddOrUpdate(u => u.UserName,
                    new ApplicationUser
                    {
                        UserName = UserNameTmp,
                        PasswordHash = password,
                        PhoneNumber = "+351221111111",
                        Email = UserNameTmp,
                        SecurityStamp = Guid.NewGuid().ToString()

                    });
                context.SaveChanges();

                //userIdInDb = context.Users.Find("");
                var example = context.Users.Single(g => g.UserName == UserNameTmp);
                string userIdNew = example.Id;

                context.User.AddOrUpdate(x => x.userID,
                new User() { userID = userIdNew, password = password, email = UserNameTmp });

                Console.WriteLine("userIdNew: " + userIdNew);
                context.Admnistrators.AddOrUpdate(x => x.name,
                new Admnistrator() { userID = userIdNew, name = UserNameTmp }
                );
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                userManager.AddToRole(userIdNew, "Admin");

                context.SaveChanges();

                context.Addresses.AddOrUpdate(x => x.addressId,
                    new Address() { name = "Rua numero 1", coordinates = 1211212 });    
                context.Addresses.AddOrUpdate(x => x.addressId,
                new Address() { name = "Rua numero 2", coordinates = 1211212 });
                context.Addresses.AddOrUpdate(x => x.addressId,
                new Address() { name = "Rua numero 3", coordinates = 1211212 });

                context.SaveChanges();


                context.RealEstates.AddOrUpdate(x => x.realEstateId,
                    new RealEstate() { area = 100, addressId = 1 ,addressLong="Apartamento Rua da Travessa 1"});
                context.RealEstates.AddOrUpdate(x => x.realEstateId,
                    new RealEstate() { area = 120, addressId = 2  ,addressLong="Apartamento Novo da Rua de Cima"});

                context.SaveChanges();


                var a = context.AdvertisementTypes.FirstOrDefault();
                if (a == null)
                {
                    context.AdvertisementTypes.AddOrUpdate(x => x.advertisementTypeID,
                    new AdvertisementType() { name = "Sale" });

                    context.AdvertisementTypes.AddOrUpdate(x => x.advertisementTypeID,
                new AdvertisementType() { name = "Purchase" });

                    context.AdvertisementTypes.AddOrUpdate(x => x.advertisementTypeID,
                new AdvertisementType() { name = "Rental" });


                    context.AdvertisementTypes.AddOrUpdate(x => x.advertisementTypeID,
                new AdvertisementType() { name = "Exchange" });

                }



                context.SaveChanges();

                



            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                var sb = new System.Text.StringBuilder();
                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new Exception(sb.ToString());
            }

        }

        //  This method will be called after migrating to the latest version.

        //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
        //  to avoid creating duplicate seed data. E.g.
        //
        //    context.People.AddOrUpdate(
        //      p => p.FullName,
        //      new Person { FullName = "Andrew Peters" },
        //      new Person { FullName = "Brice Lambson" },
        //      new Person { FullName = "Rowan Miller" }
        //    );
        //
    
    }
}
