﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Imorest.Startup))]
namespace Imorest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
