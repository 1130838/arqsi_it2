﻿var myApp = angular.module('myApp', ['ui.bootstrap','ui.materialize','angular-ranger']);
myApp.controller('WidgetCtrl', function($scope, $http) {
    
    $scope.title = "myApp";

    $http.get('http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/imoveis.php?').success(function (data) {

        $scope.lower_price_bound = 0;
        $scope.upper_price_bound = 250000;
        $scope.lower_area_bound = 0;
        $scope.upper_area_bound = 25000;
        $scope.isCollapsed = true;

        $scope.predicate = 'search_type';
        $scope.clearFilter = function () {
            console.log("cleaned");
            $scope.query = [];
        };

        $scope.awesomeRealEstates = data;

        var JsonComAcentos = JSON.stringify(data);

        var JsonSemAcentos = cleanUpSpecialChars(JsonComAcentos);

        var objSemAcentos = JSON.parse(JsonSemAcentos);
        $scope.awesomeRealEstates = objSemAcentos;

        // prevent null value prices
        var i;
        for (i = 0; i < $scope.awesomeRealEstates.length; i++) {
            if ($scope.awesomeRealEstates[i].preco == '') {
                $scope.awesomeRealEstates[i].preco = 0
            }
        }

    });

    function cleanUpSpecialChars(str) {

        str = str.replace(/[ú]/g, "u");
        str = str.replace(/[ç]/g, "c");
        str = str.replace(/[ó]/g, "o");
        str = str.replace(/[áã]/g, "a");
        return str; // final clean up
    }

    // price slider filter
    $scope.priceRange = function (item) {
        return (parseInt(item['preco']) >= $scope.lower_price_bound && parseInt(item['preco']) <= $scope.upper_price_bound);
    };
    // area slider filter
    $scope.areaRange = function (item) {
        return (parseInt(item['area']) >= $scope.lower_area_bound && parseInt(item['area']) <= $scope.upper_area_bound);
    };


});


angular.module('myApp')
    .filter('unique', function () {

        return function (items, filterOn) {

            if (filterOn === false) {
                return items;
            }

            if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
                var hashCheck = {}, newItems = [];

                var extractValueToCompare = function (item) {
                    if (angular.isObject(item) && angular.isString(filterOn)) {
                        return item[filterOn];
                    } else {
                        return item;
                    }
                };

                angular.forEach(items, function (item) {
                    var valueToCheck, isDuplicate = false;

                    for (var i = 0; i < newItems.length; i++) {
                        if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
                            isDuplicate = true;
                            break;
                        }
                    }
                    if (!isDuplicate) {
                        newItems.push(item);
                    }

                });
                items = newItems;
            }
            return items;
        };
    });



