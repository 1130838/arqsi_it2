﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Imorest.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;

namespace Imorest.Controllers
{
    public class RealEstateTypesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: RealEstateTypes1
        public ActionResult Index()
        {

            try { 
            var roleStore = new RoleStore<IdentityRole>(db);
            var roleManager = new RoleManager<IdentityRole>(roleStore);
            var userStore = new UserStore<ApplicationUser>(db);
            var userManager = new UserManager<ApplicationUser>(userStore);
            var role = roleManager.FindByName("Admin");
            //
            if (role == null)
            {
                // aqui sai em erro para pagina a criar
                return Content("There are no Admin Roles in the System!");
            }
            else
            {

                var user = userManager.FindByName(User.Identity.Name);

                if (user != null)
                {
                    var rolesForUser = userManager.GetRoles(user.Id);
                    if (rolesForUser == null || !rolesForUser.Contains("Admin"))
                    {
                        // aqui sai em erro
                        return Content("User has no Admin access!");
                    }
                }
                else
                {
                    return Content("User doesn't exist!");
                }
            }
        }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                var sb = new System.Text.StringBuilder();
                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
}

                throw new Exception(sb.ToString());
            }

            var realEstateTypes = db.RealEstateTypes.Include(r => r.RealEstateTypes);
            return View(realEstateTypes.ToList());
        }

        // GET: RealEstateTypes1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RealEstateType realEstateType = db.RealEstateTypes.Find(id);
            if (realEstateType == null)
            {
                return HttpNotFound();
            }
            return View(realEstateType);
        }

        // GET: RealEstateTypes1/Create
        public ActionResult Create()
        {
            ViewBag.RealEstateTypesID = new SelectList(db.RealEstateTypes, "realEstateTypeID", "designation");
            return View();
        }

        // POST: RealEstateTypes1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "realEstateTypeID,designation,RealEstateTypesID")] RealEstateType realEstateType)
        {
            if (ModelState.IsValid)
            {
                
                db.RealEstateTypes.Add(realEstateType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            ViewBag.RealEstateTypesID = new SelectList(db.RealEstateTypes, "realEstateTypeID", "designation", realEstateType.RealEstateTypesID);
            return View(realEstateType);
        }

        // GET: RealEstateTypes1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RealEstateType realEstateType = db.RealEstateTypes.Find(id);
            if (realEstateType == null)
            {
                return HttpNotFound();
            }
            ViewBag.RealEstateTypesID = new SelectList(db.RealEstateTypes, "realEstateTypeID", "designation", realEstateType.RealEstateTypesID);
            return View(realEstateType);
        }

        // POST: RealEstateTypes1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "realEstateTypeID,designation,RealEstateTypesID")] RealEstateType realEstateType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(realEstateType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RealEstateTypesID = new SelectList(db.RealEstateTypes, "realEstateTypeID", "designation", realEstateType.RealEstateTypesID);
            return View(realEstateType);
        }

        // GET: RealEstateTypes1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RealEstateType realEstateType = db.RealEstateTypes.Find(id);
            if (realEstateType == null)
            {
                return HttpNotFound();
            }
            return View(realEstateType);
        }

        // POST: RealEstateTypes1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RealEstateType realEstateType = db.RealEstateTypes.Find(id);
            db.RealEstateTypes.Remove(realEstateType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
