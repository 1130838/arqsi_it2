﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Imorest.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;

namespace Imorest.Controllers
{
    public class AdvertisementsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Advertisements
        public ActionResult Index()
        {
            try
            {
                var roleStore = new RoleStore<IdentityRole>(db);
                var roleManager = new RoleManager<IdentityRole>(roleStore);
                var userStore = new UserStore<ApplicationUser>(db);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var role = roleManager.FindByName("Client");
                //
                if (role == null)
                {
                    // aqui sai em erro para pagina a criar
                    return Content("There are no Client Roles in the System!");
                }
                else
                {

                    var user = userManager.FindByName(User.Identity.Name);

                    if (user != null)
                    {
                        var rolesForUser = userManager.GetRoles(user.Id);
                        if (rolesForUser == null || !rolesForUser.Contains("Client"))
                        {
                            // aqui sai em erro
                            return Content("User has no Client access!");
                        }
                    }
                    else
                    {
                        return Content("User doesn't exist!");
                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                var sb = new System.Text.StringBuilder();
                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new Exception(sb.ToString());
            }

            var advertisements = db.Advertisements.Include(a => a.AdvertisementTypes).Include(a => a.Client).Include(a => a.RealEstates).Include(a => a.realEstateType);
            return View(advertisements.ToList());
        }

        // GET: Advertisements/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advertisement advertisement = db.Advertisements.Find(id);
            if (advertisement == null)
            {
                return HttpNotFound();
            }
            return View(advertisement);
        }

        // GET: Advertisements/Create
        public ActionResult Create()
        {
            ViewBag.advertisementTypeID = new SelectList(db.AdvertisementTypes, "advertisementTypeID", "name");

            //ViewBag.clientId = new SelectList(db.Clients, "clientId", "name");
            var userStore = new UserStore<ApplicationUser>(db);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var user = userManager.FindByName(User.Identity.Name);

            //
            var list = db.Clients.Where(a => a.userID == user.Id);
            
            var selectClient = new SelectList(list, "clientId", "name");
            ViewBag.clientId = selectClient;

            ViewBag.realEstateId = new SelectList(db.RealEstates, "realEstateId", "addressLong");
            ViewBag.realEstateTypeID = new SelectList(db.RealEstateTypes, "realEstateTypeID", "designation");
            return View();
        }

        // POST: Advertisements/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "advertisementID,advertisementRef,realEstateId,price,clientId,realEstateTypeID,advertisementTypeID")] Advertisement advertisement)
        {

            var userStore = new UserStore<ApplicationUser>(db);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var user = userManager.FindByName(User.Identity.Name);
            //var client = db.Clients.Where(a => a.userID == user.Id);
            var client = db.Clients.Single(c => c.userID == user.Id);

            if (ModelState.IsValid)
            {
                // ir a view retirar o clientId
                // aqui atribuir o useriID
                advertisement.clientId = client.clientId;
                db.Advertisements.Add(advertisement);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.advertisementTypeID = new SelectList(db.AdvertisementTypes, "advertisementTypeID", "name", advertisement.advertisementTypeID);

            //ViewBag.clientId = new SelectList(db.Clients, "clientId", "name", advertisement.clientId);

            user = userManager.FindByName(User.Identity.Name);

            //
            var list = db.Clients.Where(a => a.userID == user.Id);
            var selectClient = new SelectList(list, "clientId", "name");
            ViewBag.clientId = selectClient;

            ViewBag.realEstateId = new SelectList(db.RealEstates, "realEstateId", "addressLong", advertisement.realEstateId);
            ViewBag.realEstateTypeID = new SelectList(db.RealEstateTypes, "realEstateTypeID", "designation", advertisement.realEstateTypeID);
            return View(advertisement);
        }

        // GET: Advertisements/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advertisement advertisement = db.Advertisements.Find(id);
            if (advertisement == null)
            {
                return HttpNotFound();
            }
            ViewBag.advertisementTypeID = new SelectList(db.AdvertisementTypes, "advertisementTypeID", "name", advertisement.advertisementTypeID);

            //ViewBag.clientId = new SelectList(db.Clients, "clientId", "name", advertisement.clientId);
            var userStore = new UserStore<ApplicationUser>(db);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var user = userManager.FindByName(User.Identity.Name);

            //
            var list = db.Clients.Where(a => a.userID == user.Id);
            var selectClient = new SelectList(list, "clientId", "name");
            ViewBag.clientId = selectClient;

            ViewBag.realEstateId = new SelectList(db.RealEstates, "realEstateId", "addressLong", advertisement.realEstateId);
            ViewBag.realEstateTypeID = new SelectList(db.RealEstateTypes, "realEstateTypeID", "designation", advertisement.realEstateTypeID);
            return View(advertisement);
        }

        // POST: Advertisements/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "advertisementID,advertisementRef,realEstateId,price,clientId,realEstateTypeID,advertisementTypeID")] Advertisement advertisement)
        {

            var userStore = new UserStore<ApplicationUser>(db);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var user = userManager.FindByName(User.Identity.Name);
            //var list = db.Clients.Where(a => a.userID == user.Id);
            var client = db.Clients.Single(c=> c.userID == user.Id);

            if (ModelState.IsValid)
            {
                // INACIO

                advertisement.clientId = client.clientId;
                db.Entry(advertisement).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.advertisementTypeID = new SelectList(db.AdvertisementTypes, "advertisementTypeID", "name", advertisement.advertisementTypeID);

            //ViewBag.clientId = new SelectList(db.Clients, "clientId", "name", advertisement.clientId);
           

            user = userManager.FindByName(User.Identity.Name);

            //
            var list = db.Clients.Where(a => a.userID == user.Id);
            var selectClient = new SelectList(list, "clientId", "name");
            ViewBag.clientId = selectClient;

            ViewBag.realEstateId = new SelectList(db.RealEstates, "realEstateId", "addressLong", advertisement.realEstateId);
            ViewBag.realEstateTypeID = new SelectList(db.RealEstateTypes, "realEstateTypeID", "designation", advertisement.realEstateTypeID);
            return View(advertisement);
        }

        // GET: Advertisements/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Advertisement advertisement = db.Advertisements.Find(id);
            if (advertisement == null)
            {
                return HttpNotFound();
            }
            return View(advertisement);
        }

        // POST: Advertisements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Advertisement advertisement = db.Advertisements.Find(id);
            db.Advertisements.Remove(advertisement);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
