﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Imorest.Models;

namespace Imorest.Controllers
{
    public class RealEstatesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: RealEstates
        public ActionResult Index()
        {
            var realEstates = db.RealEstates.Include(r => r.Address);
            return View(realEstates.ToList());
        }

        // GET: RealEstates/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RealEstate realEstate = db.RealEstates.Find(id);
            if (realEstate == null)
            {
                return HttpNotFound();
            }
            return View(realEstate);
        }

        // GET: RealEstates/Create
        public ActionResult Create()
        {
            ViewBag.addressId = new SelectList(db.Addresses, "addressId", "name");
            return View();
        }

        // POST: RealEstates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "realEstateId,area,addressId,addressLong")] RealEstate realEstate)
        {
            if (ModelState.IsValid)
            {
                db.RealEstates.Add(realEstate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.addressId = new SelectList(db.Addresses, "addressId", "name", realEstate.addressId);
            return View(realEstate);
        }

        // GET: RealEstates/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RealEstate realEstate = db.RealEstates.Find(id);
            if (realEstate == null)
            {
                return HttpNotFound();
            }
            string fullPath = Request.MapPath("~/img/");
            ViewBag.imagePath = 
            ViewBag.addressId = new SelectList(db.Addresses, "addressId", "name", realEstate.addressId);
            return View(realEstate);
        }

        // POST: RealEstates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "realEstateId,area,addressId,addressLong")] RealEstate realEstate)
        {
            if (ModelState.IsValid)
            {
                db.Entry(realEstate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.addressId = new SelectList(db.Addresses, "addressId", "name", realEstate.addressId);
            return View(realEstate);
        }

        // GET: RealEstates/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RealEstate realEstate = db.RealEstates.Find(id);
            if (realEstate == null)
            {
                return HttpNotFound();
            }
            return View(realEstate);
        }

        // POST: RealEstates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RealEstate realEstate = db.RealEstates.Find(id);
            db.RealEstates.Remove(realEstate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult UploadPhoto(string cakesImage,
        HttpPostedFileBase photo)
        {
            string fullPath = Request.MapPath("~/img/"
            + cakesImage);
            //string path = @"~\img\" +cakesImage;

            if (photo != null)
                photo.SaveAs(fullPath);

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePhoto(string photoFileName)
        {
            //Session["DeleteSuccess"] = "No";
            var photoName = "";
            photoName = photoFileName;
            string fullPath = Request.MapPath("~/img/"
            + photoName);

            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
                //Session["DeleteSuccess"] = "Yes";
            }
            return RedirectToAction("Index");
        }

    }
}
