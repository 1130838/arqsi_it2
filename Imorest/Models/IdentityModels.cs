﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Imorest.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateuserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            
        }
        public DbSet<RealEstateType> RealEstateTypes { get; set; }

        public DbSet<Alert> Alerts { get; set; }

        public DbSet<Admnistrator> Admnistrators { get; set; }

        public DbSet<Advertisement> Advertisements { get; set; }

        public DbSet<Client> Clients { get; set; }

        public DbSet<RealEstate> RealEstates { get; set; }
        public DbSet<User> User { get; set; }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<AdvertisementType> AdvertisementTypes { get; set; }

        public DbSet<Parameter> Parameters { get; set; }
        

        public static ApplicationDbContext Create()
        {
            
            return new ApplicationDbContext();
        }
        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    //...
        //    modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        //    //modelBuilder.Conventions.Remove<OneToManyCascadeUpdateConvention>();
        //}

    }
}