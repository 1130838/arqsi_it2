﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Imorest.Models
{
    public class Admnistrator

    {
        
        public int admnistratorID { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name = "Name: ")]
        public string name { get; set; }
        public string userID { get; set; }

        public virtual User user { get; set; }

}
}