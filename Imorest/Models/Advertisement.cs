﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Imorest.Models
{
    public class Advertisement
    {
        
        public int advertisementID { get; set; }
        public string advertisementRef { get; set; }

        public int realEstateId { get; set; }
        public virtual RealEstate RealEstates { get; set; }

        public float price { get; set; }

        public int clientId { get; set; }
        public virtual Client Client { get; set; }

        public int realEstateTypeID { get; set; }

        public virtual RealEstateType realEstateType { get; set; }

        public int advertisementTypeID { get; set; }

        public virtual AdvertisementType AdvertisementTypes { get; set; }



    }
}