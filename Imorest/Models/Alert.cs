﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Imorest.Models
{
    public class Alert
    {
        
        public int alertID { get; set; }
        
        public int clientId { get; set; }
        public virtual Client Client { get; set; }

        [RegularExpression("^([a-zA-Z0-9 .&'-]+)$", ErrorMessage = "Invalid designation")]
        [Required]
        [StringLength(100)]
        public string alertRef  { get; set; }

        public ICollection<Parameter> Parameters { get; set; }

    }
}