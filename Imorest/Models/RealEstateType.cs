﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Imorest.Models
{
    public class RealEstateType

    {
        
        public int realEstateTypeID { get; set; }

        [RegularExpression("^([a-zA-Z0-9 .&'-]+)$", ErrorMessage = "Invalid designation")]
        [Required]
        [StringLength(100)]
        public string designation { get; set; }
        [ForeignKey("RealEstateTypes")]
        public int? RealEstateTypesID { get; set; }
        public virtual RealEstateType RealEstateTypes { get; set; }


    }
}