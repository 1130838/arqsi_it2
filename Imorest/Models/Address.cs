﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Imorest.Models
{
    public class Address 
    {

     
        public int addressId { get; set; }

        [RegularExpression("^([a-zA-Z0-9 .&'-]+)$", ErrorMessage = "Invalid Name")]
        [Required]
        [StringLength(100)]
        public string name { get; set; }
		
        public float coordinates { get; set; }
    }
}