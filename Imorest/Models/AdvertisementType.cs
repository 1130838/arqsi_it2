﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Imorest.Models
{
    public class AdvertisementType
    {
       
        public int advertisementTypeID { get; set; }
        

        [RegularExpression(@"^[A-Z]+[a-zA-Z''-'\s]*$")]
        [Required]
        [StringLength(100)]
        public string name { get; set; }

    }
}