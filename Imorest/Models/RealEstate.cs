﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Imorest.Models
{
    public class RealEstate
    {
        
        public int realEstateId { get; set; }
        
        public float area  { get; set; }
        public int addressId { get; set; }
        public virtual Address Address { get; set; }

        public string addressLong { get; set; }

        public string RealEstateImage
        {
            get { return addressLong.Replace(" ", string.Empty) + ".jpg"; } 
        }

    }
}