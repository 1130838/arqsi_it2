﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Imorest.Models
{
    public class Client
    {
        
        public int clientId { get; set;    }

        [Required(ErrorMessage = "Name is required")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [Display(Name = "Name: ")]
        public string name { get; set; }

        [RegularExpression("^([a-zA-Z0-9 .&'-]+)$", ErrorMessage = "Invalid Address")]
        [StringLength(100)]
        public string address { get; set; }
        public string userID { get; set; }
        public User user { get; set; }

    }
}