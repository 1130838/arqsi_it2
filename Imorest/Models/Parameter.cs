﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Imorest.Models
{
    public class Parameter
    {
        
        public int parameterID { get; set; }

        [RegularExpression("^([a-zA-Z0-9 .&'-]+)$", ErrorMessage = "Invalid Name")]
        [Required]
        [StringLength(100)]
        public string name { get; set; }

        [Range(1, 9999999)]
        [DataType(DataType.Currency)]
        public float price { get; set; }


        public float area { get; set; }

        public int AlertID { get; set; }
        public virtual Alert Alert { get; set; }

    }
}